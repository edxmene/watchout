using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.GeoJsonObjectModel;
using System;

public class Restaurant
{
    public ObjectId Id { get; set; }
    public GeoJsonPoint<GeoJson2DCoordinates> Location { get; set; }
    public string Name { get; set; }
}

public class Neighborhood
{
    public ObjectId Id { get; set; }
    public GeoJsonPoint<GeoJson2DCoordinates> Geometry { get; set; }
    public string Name { get; set; }
}

// Be sure to update yourUsername, yourPassword, yourClusterName, and yourProjectId to your own! 
// Similarly, also update "sample-geo", "restaurants", and "neighborhoods" to whatever you've named your database and collections.
var client = new MongoClient("mongodb+srv://develop:jalasoft@cluster0.wdsmm.mongodb.net/myFirstDatabase?retryWrites=true&w=majority");
var database = client.GetDatabase("sample-geo");
var restaurantCollection = database.GetCollection<Restaurant>("restaurants");
var neighborhoodCollection = database.GetCollection<Neighborhood>("neighborhoods");


static void Log(string exampleName, FilterDefinition<Restaurant> filter)
{
    var serializerRegistry = BsonSerializer.SerializerRegistry;
    var documentSerializer = serializerRegistry.GetSerializer<Restaurant>();
    var rendered = filter.Render(documentSerializer, serializerRegistry);
    Console.WriteLine($"{exampleName} example:");
    Console.WriteLine(rendered.ToJson(new JsonWriterSettings { Indent = true }));
    Console.WriteLine();
}
