// <copyright file="Query.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Application.Services.Contracts;
using Domain.Entities;
using Domain.Models;

namespace GraphQL;

[GraphQLDescription("Get all the incidents.")]
public class Query
{
    private readonly IPlaceIncidentService _placeIncidentService;
    
    public Query([Service] IPlaceIncidentService placeIncidentService)
    {
        _placeIncidentService = placeIncidentService ?? throw new ArgumentNullException(nameof(placeIncidentService)); ;
    }

    [UseProjection]
    [UseSorting]
    [UseFiltering]
    public async Task<IExecutable<PlaceIncidentModel>> GetPlaceIncidents()
    {
        var places = await _placeIncidentService.GetPlacesIncidentAsync().ConfigureAwait(true);
        return places.AsExecutable();
    }

    [UseProjection]
    [UseSorting]
    [UseFiltering]
    public async Task<IExecutable<PlaceIncidentModel>> GetPlacesNear(double latitude, double longitude, int maxDistance, int minDistance)
    {
        var places = await _placeIncidentService.GetPlacesNearAsync(latitude, longitude, maxDistance, minDistance).ConfigureAwait(true);
        return places.AsExecutable();
    }

    [UseProjection]
    [UseSorting]
    [UseFiltering]
    public async Task<IExecutable<PlaceIncidentModel>> GetPlacesWithin(List<Coordinates> coordinates)
    {
        var places = await _placeIncidentService.GetPlacesWithinAsync(coordinates).ConfigureAwait(true);
        return places.AsExecutable();
    }

    [UseProjection]
    [UseSorting]
    [UseFiltering]
    public async Task<IExecutable<PlaceIncidentModel>> GetPlacesWithinCenter(double latitude, double longitude, int distance)
    {
        var places = await _placeIncidentService.GetPlacesWithinCenterAsync(latitude, longitude, distance).ConfigureAwait(true);
        return places.AsExecutable();
    }

}
