// <copyright file="Subscription.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Domain.Models;

namespace GraphQL;

/// <summary>
/// Represents the subscriptions available.
/// </summary>
///

[GraphQLDescription("Represents the queries available.")]
public class Subscription
{
    [Subscribe]
    [Topic]
    [GraphQLDescription("The subscription for added place incident.")]
    public PlaceIncidentModel OnPlaceIncidentAdded([EventMessage] PlaceIncidentModel placeIncident)
    {
        return placeIncident;
    }
}

