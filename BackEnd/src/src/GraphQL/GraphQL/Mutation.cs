// <copyright file="Mutation.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Application.Services.Contracts;
using Domain.Models;
using GraphQL.Types.PlaceIncident;
using HotChocolate.Subscriptions;

namespace GraphQL;

public class Mutation
{
    private readonly IPlaceIncidentService _placeIncidentService;
    
    public Mutation([Service] IPlaceIncidentService placeIncidentService)
    {
        _placeIncidentService = placeIncidentService ?? throw new ArgumentNullException(nameof(placeIncidentService)); ;
    }

    public async Task<PlaceIncidentPayload> AddPlaceIncidentAsync(
        PlaceIncidentInput input,
        [Service] ITopicEventSender eventSender,
        CancellationToken cancellationToken)
    {
        var placeIncidentModel = new PlaceIncidentModel
        {
            Owner = input.Owner,
            Description = input.Description,
            Severity = input.Severity,
            DurationTime = input.DurationTime,
            Category = input.Category,
            Location = input.Location,
            Geometry = input.Geometry
        };

        var newPlaceIncident = await _placeIncidentService
            .AddPlaceIncidentAsync(placeIncidentModel)
            .ConfigureAwait(true);

        await eventSender.SendAsync(nameof(Subscription.OnPlaceIncidentAdded),
            placeIncidentModel,
            cancellationToken)
            .ConfigureAwait(true);

        return new PlaceIncidentPayload(newPlaceIncident);
    }

    public async Task<PlaceIncidentPayload> UpdatePlaceIncidentAsync(string id, PlaceIncidentInput input, CancellationToken cancellationToken)
    {
        var placeIncidentModel = new PlaceIncidentModel
        {
            Owner = input.Owner,
            Description = input.Description,
            Severity = input.Severity,
            DurationTime = input.DurationTime,
            Category = input.Category,
            Location = input.Location,
            Geometry = input.Geometry
        };

        await _placeIncidentService.UpdatePlaceIncidentAsync(id, placeIncidentModel).ConfigureAwait(true);

        return new PlaceIncidentPayload(placeIncidentModel);
    }

    public async Task<DeletePlaceIncidetePayload> DeletePlaceIncidentAsync(string id, CancellationToken cancellationToken)
    {
        var response = await _placeIncidentService.DeletePlaceIncidentAsync(id).ConfigureAwait(true);
        return new DeletePlaceIncidetePayload(response);
    }
}

