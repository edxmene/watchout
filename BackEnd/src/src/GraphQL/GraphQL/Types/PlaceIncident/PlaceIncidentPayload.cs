// <copyright file="PlaceIncidentPayload.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Domain.Models;

namespace GraphQL.Types.PlaceIncident;

public record PlaceIncidentPayload(PlaceIncidentModel placeIncident);
