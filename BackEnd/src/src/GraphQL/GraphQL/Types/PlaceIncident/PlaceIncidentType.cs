// <copyright file="PlaceIncidentType.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Domain.Models;

namespace GraphQL.Types.PlaceIncident;

public class PlaceIncidentType : ObjectType<PlaceIncidentModel>
{
    protected override void Configure(IObjectTypeDescriptor<PlaceIncidentModel> descriptor)
    {
        base.Configure(descriptor);
        descriptor.Description("Represents any software or service that has a command line interface");

        descriptor
           .Field(p => p.Id)
           .Description("Represents the unique ID for the place incident.");

        descriptor
           .Field(p => p.Owner)
           .Description("Represents the user that create the place incident.");

       descriptor
           .Field(p => p.Description)
           .Description("Represents a little description of the incident.");

        descriptor
           .Field(p => p.Severity)
           .Description("Represent the severity of the incident.");

        descriptor
           .Field(p => p.DurationTime)
           .Description("Represents the duration of the incident.");

        descriptor
           .Field(p => p.Category)
           .Description("Represents the category of the incident.");

        descriptor
           .Field(p => p.Location)
           .Description("Represents the type of the location of the incident.");

        descriptor
           .Field(p => p.Geometry)
           .Description("Represents the type of the location of the incident.");
    }
}
