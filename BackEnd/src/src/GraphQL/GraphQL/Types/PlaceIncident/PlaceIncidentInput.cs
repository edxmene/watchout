// <copyright file="PlaceIncidentInput.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Domain.Entities;
//using Domain.Enums;

namespace GraphQL.Types.PlaceIncident;

public class PlaceIncidentInput
{
    public string? Owner { get; set; }
    public string Description { get; set; }
    //public SeverityType Severity { get; set; }
    public string Severity { get; set; }
    public int DurationTime { get; set; }
    //public CategoryType Category { get; set; }
    public string Category { get; set; }
    public Domain.Entities.Location? Location { get; set; }
        
    public Geometry? Geometry { get; set; }

    public PlaceIncidentInput(
        string owner,
        string description,
        //SeverityType severity,
        string severity,
        int durationTime,
        string category,
        //CategoryType category,
        Domain.Entities.Location location,
        Geometry geometry
        )
    {
        Owner = owner;
        Description = description;
        Severity = severity;
        DurationTime = durationTime;
        Category = category;
        Location = location;
        Geometry = geometry;
    }
}
