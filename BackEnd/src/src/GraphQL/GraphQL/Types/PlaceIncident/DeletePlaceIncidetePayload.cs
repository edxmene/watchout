// <copyright file="DeletePlaceIncidetePayload.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Application.Responses;

namespace GraphQL.Types.PlaceIncident;

public record DeletePlaceIncidetePayload(GenericResponse response);

