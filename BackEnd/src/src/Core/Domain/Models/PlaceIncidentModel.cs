// <copyright file="PlaceIncidentModel.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Domain.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Domain.Models;

[BsonIgnoreExtraElements]

public class PlaceIncidentModel
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string? Id { get; set; }
    public string? Owner { get; set; }
    public string? Description { get; set; }
    public string? Severity { get; set; }
    public int DurationTime { get; set; }
    public string? Category { get; set; }
    public Location? Location { get; set; }
    public Geometry? Geometry { get; set; }
}
