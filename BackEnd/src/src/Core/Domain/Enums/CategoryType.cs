// <copyright file="CategoryType.cs" company="Watchout">
// Copyright (c) 2022</copyright>

namespace Domain.Enums;

public enum CategoryType
{
    NaturalDisaster,
    TrafficIncident,
    SocialIncident,
    DangerZone
}
