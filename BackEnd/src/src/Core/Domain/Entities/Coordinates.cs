// <copyright file="Coordinates.cs" company="Watchout">
// Copyright (c) 2022</copyright>

namespace Domain.Entities;

public record Coordinates(double latitude, double longitude);

