// <copyright file="Location.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Newtonsoft.Json;

namespace Domain.Entities;

public class Location
{
    [JsonProperty("coordinates")]
    public double[]? Coordinates { get; set; }
    [JsonProperty("type")]
    public string Type { get; set; } = null!;
}
