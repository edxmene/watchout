// <copyright file="Geometry.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Newtonsoft.Json;

namespace Domain.Entities;

public class Geometry
{
    [JsonProperty("coordinates")]
    public List<double[]>? Coordinates { get; set; }
    [JsonProperty("type")]
    public string Type { get; set; } = null!;
}
