// <copyright file="User.cs" company="Watchout">
// Copyright (c) 2022</copyright>

namespace Domain.Entities;

public class User
{
    public string Id { get; set; } = null!;
    public string AuthToken { get; set; } = null!;
    public string Email { get; set; } = null!;
    public string FirstName { get; set; } = null!;
    public string LastName { get; set; } = null!;
    public byte[] PasswordSalt { get; set; } = null!;
    public byte[] PasswordHash { get; set; } = null!;
    public bool Active { get; set; } = true;
}
