// <copyright file="UserService.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using System.Security.Cryptography;
using Application.Contracts;
using Application.DTOs;
using Application.Helpers;
using Application.Responses;
using Application.Services.Contracts;
using AutoMapper;
using Domain.Models;
using Domain.Responses;
using Microsoft.Extensions.Options;

namespace Application.Services;

public class UserService : IUserService
{
    private readonly IUserRepository _userRepository;
    private readonly IMapper _mapper;
    private readonly IOptions<JwtAuthentication> _jwtAuthentication;
    private readonly IOptions<SendEmail> _sendEmail;

    public UserService(IUserRepository userRepository, IMapper mapper, IOptions<JwtAuthentication> jwtAuthentication, IOptions<SendEmail> sendEmail)
    {
        _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        _mapper = mapper;
        _jwtAuthentication = jwtAuthentication ?? throw new ArgumentNullException(nameof(jwtAuthentication));
        _sendEmail = sendEmail ?? throw new ArgumentNullException(nameof(sendEmail));
    }

    private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
    {
        using var hmac = new HMACSHA512();
        passwordSalt = hmac.Key;
        passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
    }

    private static bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
    {
        using var hmac = new HMACSHA512(passwordSalt);
        var computeHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
        return computeHash.SequenceEqual(passwordHash);
    }

    public async Task<UserResponse> AddUserAsync(UserDTO userDto)
    {
        try
        {
            //TODO: Tech debt, mapper not working, manually mapping
            //var entity = _mapper.Map<UserModel>(userDto);
            var entity = new UserModel();
            entity.Email = userDto.Email;
            entity.FirstName = userDto.FirstName;
            entity.LastName = userDto.LastName;
            CreatePasswordHash(userDto.Password, out byte[] passwordHash, out byte[] passwordSalt);
            entity.PasswordHash = passwordHash;
            entity.PasswordSalt = passwordSalt;
            entity.AuthToken = _jwtAuthentication.Value.GenerateToken(entity);

            var newUser = await GetUserAsync(entity.Email).ConfigureAwait(true);
            if (newUser != null)
            {
                return new UserResponse(409, "user already exists.");
            }
            await _userRepository.Add(entity).ConfigureAwait(true);
            return new UserResponse(201, entity.Id, entity.AuthToken, "user created successfully");

        }
        catch (Exception ex)
        {
            return ex.Message.StartsWith("mongoerror: e11000 duplicate key error")
                    ? new UserResponse(409, "a user with the given email already exists.")
                    : new UserResponse(409, ex.Message);
        }
    }

    public async Task<UserResponse> AddUserGoogleAsync(UserGoogleDTO userGoogleDto)
    {
        try
        {
            var entity = new UserModel();
            entity.Email = userGoogleDto.Email;
            entity.FirstName = userGoogleDto.FirstName;
            entity.LastName = userGoogleDto.LastName;
            entity.AuthToken = _jwtAuthentication.Value.GenerateToken(entity);
            entity.Google = userGoogleDto.Google;

            var newUser = await GetUserAsync(entity.Email).ConfigureAwait(true);
            if (newUser != null)
            {
                return new UserResponse(409, "user already exists.");
            }
            await _userRepository.Add(entity).ConfigureAwait(true);
            return new UserResponse(201, entity.Id, entity.AuthToken, "user created successfully");
        }
        catch (Exception ex)
        {
            return ex.Message.StartsWith("mongoerror: e11000 duplicate key error")
                    ? new UserResponse(409, "a user with the given email already exists.")
                    : new UserResponse(409, ex.Message);
        }
    }

    public async Task<List<UserModel>> GetAllUsersAsync()
    {
        var users = await _userRepository.GetAll().ConfigureAwait(true);
        return users;
    }

    public async Task<UserModel> GetUserAsync(string email)
    {
        var user = await _userRepository.GetUserAsync(email).ConfigureAwait(true);
        return user;
    }

    public async Task<LoginResponse> LoginUserAsync(LoginDTO userLogin)
    {
        var user = await GetUserAsync(userLogin.Email).ConfigureAwait(true);
        if (user == null || !VerifyPasswordHash(userLogin.Password, user.PasswordHash, user.PasswordSalt))
        {
            return new LoginResponse(false);
        }

        user.AuthToken = _jwtAuthentication.Value.GenerateToken(user);
        return new LoginResponse(true, user.Id, user.AuthToken);
    }

    public async Task<GenericResponse> UpdateUserAsync(string email, UserDTO userDto)
    {
        var user = await _userRepository.GetUserAsync(email).ConfigureAwait(true);
        if (user == null)
        {
            var response = new GenericResponse("User dont Exist", false);
            return response;
        }
        if (!VerifyPasswordHash(userDto.Password, user.PasswordHash, user.PasswordSalt))
        {
            var response = new GenericResponse("Password wrong", false);
            return response;
        }
        if (userDto.NewPassword != null)
        {
            CreatePasswordHash(userDto.NewPassword, out byte[] passwordHash, out byte[] passwordSalt);
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
        }
        user.FirstName = userDto.FirstName;
        user.LastName = userDto.LastName;
        user.AuthToken = _jwtAuthentication.Value.GenerateToken(user);
        _userRepository.Update(user);
        return new GenericResponse("user update", true);

    }

    public async Task<GenericResponse> DeleteUserAsync(string email)
    {
        var user = await _userRepository.GetUserAsync(email).ConfigureAwait(true);
        if (user == null)
        {
            var response = new GenericResponse("User dont Exist", false);
            return response;
        }
        _userRepository.Delete(user);
        return new GenericResponse("user Delete", true);
       
    }

    public async Task<GenericResponse> RecoveryPasswordAsync(SendEmailDTO email)
    {
        var user = await _userRepository.GetUserAsync(email.Email).ConfigureAwait(true);
        if (user == null)
        {
            return new GenericResponse("User doesn't exist", false);
        }
        else
        {
            user.AuthToken = _jwtAuthentication.Value.GenerateToken(user);
            await _sendEmail.Value.SendEmailsAsync(user.Email, user.AuthToken).ConfigureAwait(true);
            return new GenericResponse("Email sent", true);
        }
    }

    public async Task<GenericResponse> ResetPasswordAsync(string authToken, ResetPasswordDTO userNewPassword)
    {
        var userEmail = _jwtAuthentication.Value.DecodeToken(authToken);
        var user = await _userRepository.GetUserAsync(userEmail).ConfigureAwait(true);
        if (user == null)
        {
            return new GenericResponse("Token doesn't belong to any user", false);
        }
        else
        {
            CreatePasswordHash(userNewPassword.NewPassword, out byte[] passwordHash, out byte[] passwordSalt);
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            user.AuthToken = _jwtAuthentication.Value.GenerateToken(user);
            _userRepository.Update(user);
            return new GenericResponse("Password reset", true);
        }

    }
}
