// <copyright file="PlaceIncidentService.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Application.Contracts;
using Application.Responses;
using Application.Services.Contracts;
using Domain.Entities;
using Domain.Models;

namespace Application.Services;

public class PlaceIncidentService : IPlaceIncidentService
{
    private readonly IPlaceIncidentRepository _placeIncidentRepository;
    
    public PlaceIncidentService(IPlaceIncidentRepository placeIncidentRepository)
    {
        _placeIncidentRepository = placeIncidentRepository;
    }
    public async Task<PlaceIncidentModel> AddPlaceIncidentAsync(PlaceIncidentModel placeIncidentModel)
    {
        return await _placeIncidentRepository.Add(placeIncidentModel).ConfigureAwait(true);
    }

    public async Task<List<PlaceIncidentModel>> GetPlacesIncidentAsync()
    {
        var places = await _placeIncidentRepository.GetAll().ConfigureAwait(true);
        return places;
    }

    public async Task<GenericResponse> UpdatePlaceIncidentAsync(string id, PlaceIncidentModel placeIncidentInput)
    {
        var placeIncident = await _placeIncidentRepository.GetPlaceIncidentAsync(id).ConfigureAwait(true);
        if (placeIncident == null)
        {
            var response = new GenericResponse("Place incident don't Exist", false);
            return response;
        }
        placeIncident.Description = placeIncidentInput.Description;
        placeIncident.Severity = placeIncidentInput.Severity;
        placeIncident.Category = placeIncidentInput.Category;
        placeIncident.DurationTime = placeIncidentInput.DurationTime;

        _placeIncidentRepository.Update(placeIncident);
        return new GenericResponse("Place incident update", true);

    }

    public async Task<GenericResponse> DeletePlaceIncidentAsync(string id)
    {
        var placeIncident = await _placeIncidentRepository.GetPlaceIncidentAsync(id).ConfigureAwait(true);
        if (placeIncident == null)
        {
            var response = new GenericResponse("Place incident don't Exist", false);
            return response;
        }
        _placeIncidentRepository.Delete(placeIncident);
        return new GenericResponse("Place Incident Delete", true);

    }

    public async Task<List<PlaceIncidentModel>> GetPlacesNearAsync(double latitude, double longitude, int maxDistance, int minDistance)
    {
        var places = await _placeIncidentRepository.GetPlaceIncidentByNear(latitude, longitude, maxDistance, minDistance).ConfigureAwait(true);
        return places;
    }

    public async Task<List<PlaceIncidentModel>> GetPlacesWithinAsync(List<Coordinates> coordinates)
    {
        var places = await _placeIncidentRepository.GetPlaceIncidentByWithin(coordinates).ConfigureAwait(true);
        return places;
    }

    public async Task<List<PlaceIncidentModel>> GetPlacesWithinCenterAsync(double latitude, double longitude, int distance)
    {
        var places = await _placeIncidentRepository.GetPlaceIncidentByWithinCenter(latitude, longitude, distance).ConfigureAwait(true);
        return places;
    }

    public async Task<List<PlaceIncidentModel>> GetPlacesWithinCenterSphereAsync(double latitude, double longitude, int distance)
    {
        var places = await _placeIncidentRepository.GetPlaceIncidentByWithinCenterSphere(latitude, longitude, distance).ConfigureAwait(true);
        return places;
    }

    public async Task<List<PlaceIncidentModel>> GetPlacesIntersectsAsync(double latitude, double longitude)
    {
        var places = await _placeIncidentRepository.GetPlaceIncidentByIntersects(latitude, longitude).ConfigureAwait(true);
        return places;
    }
}
