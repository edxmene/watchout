// <copyright file="IUserService.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Application.DTOs;
using Application.Responses;
using Domain.Models;
using Domain.Responses;

namespace Application.Services.Contracts;

public interface IUserService
{
    Task<UserResponse> AddUserAsync(UserDTO userDto);
    Task<UserResponse> AddUserGoogleAsync(UserGoogleDTO userGoogleDto);
    Task<UserModel> GetUserAsync(string email);
    Task<List<UserModel>> GetAllUsersAsync();
    Task<LoginResponse> LoginUserAsync(LoginDTO userLogin);
    Task<GenericResponse> UpdateUserAsync(string email, UserDTO userDto);
    Task<GenericResponse> DeleteUserAsync(string email);
    Task<GenericResponse> RecoveryPasswordAsync(SendEmailDTO email);
    Task<GenericResponse> ResetPasswordAsync(string authToken, ResetPasswordDTO newPassword);
}
