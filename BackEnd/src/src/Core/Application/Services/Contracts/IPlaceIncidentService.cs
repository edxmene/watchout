// <copyright file="IPlaceIncidentService.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Application.Responses;
using Domain.Entities;
using Domain.Models;

namespace Application.Services.Contracts;

public interface IPlaceIncidentService
{
    Task<PlaceIncidentModel> AddPlaceIncidentAsync(PlaceIncidentModel placeIncidentModel);
    Task<List<PlaceIncidentModel>> GetPlacesIncidentAsync();
    Task<GenericResponse> DeletePlaceIncidentAsync(string id);
    Task<GenericResponse> UpdatePlaceIncidentAsync(string id, PlaceIncidentModel placeIncidentInput);
    Task<List<PlaceIncidentModel>> GetPlacesNearAsync(double latitud, double longitud, int maxDistance, int minDistance);
    Task<List<PlaceIncidentModel>> GetPlacesWithinAsync(List<Coordinates> coordinates);
    Task<List<PlaceIncidentModel>> GetPlacesWithinCenterAsync(double latitud, double longitud, int distance);
    Task<List<PlaceIncidentModel>> GetPlacesWithinCenterSphereAsync(double latitud, double longitud, int distance);
    Task<List<PlaceIncidentModel>> GetPlacesIntersectsAsync(double latitud, double longitud);

}
