// <copyright file="UserDTO.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Newtonsoft.Json;

namespace Application.DTOs;

public class UserDTO
{
    [JsonProperty("auth_token")]

    public string FirstName { get; set; } = null!;
    public string LastName { get; set; } = null!;
    public string Email { get; set; } = null!;
    public string Password { get; set; } = null!;
    public string? NewPassword { get; set; }
}
