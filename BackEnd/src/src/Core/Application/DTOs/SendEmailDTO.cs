// <copyright file="SendEmailDTO.cs" company="Watchout">
// Copyright (c) 2022</copyright>

namespace Application.DTOs;
public class SendEmailDTO
{
    public string Email { get; set; } = null!;
}
