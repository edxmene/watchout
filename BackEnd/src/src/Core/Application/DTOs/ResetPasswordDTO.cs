// <copyright file="PasswordDTO.cs" company="Watchout">
// Copyright (c) 2022</copyright>

namespace Application.DTOs;
public class ResetPasswordDTO
{
    public string NewPassword { get; set; } = null!;
}
