// <copyright file="UserResponse.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Newtonsoft.Json;

namespace Domain.Responses;

public class UserResponse
{
    [JsonProperty("auth_token")]
    public string AuthToken { get; set; } = null!;
    public string Message { get; set; } = null!;
    public int Status { get; set; }
    public string Id { get; set; } = null!;

    public UserResponse(int status, string message)
    {
        Status = status;
        Message = message;
    }

    public UserResponse(int status, string id, string authToken, string message)
    {
        Status = status;
        Id = id;
        AuthToken = authToken;
        Message = message;
    }

}
