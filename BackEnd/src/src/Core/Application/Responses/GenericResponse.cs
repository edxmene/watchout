// <copyright file="GenericResponse.cs" company="Watchout">
// Copyright (c) 2022</copyright>

namespace Application.Responses;

public class GenericResponse
{
   
    public string Message { get; set; }
    public bool Result { get; set; }
  
    

    public GenericResponse(string message,bool result)
    {
        Message = message;
        Result = result;
    }

}
