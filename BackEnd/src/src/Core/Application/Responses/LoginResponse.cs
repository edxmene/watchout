// <copyright file="LoginResponse.cs" company="Rocco Company">
// Copyright (c) 2022, Heliberto Arias
// </copyright>

using Newtonsoft.Json;

namespace Application.Responses;

public class LoginResponse
{
    [JsonProperty("auth_token")]
    public string AuthToken { get; set; } = null!;
    public bool Login { get; set; }
    public string Id { get; set; } = null!;

    public LoginResponse(bool login, string id, string authToken)
    {
        Id = id;
        AuthToken = authToken;
        Login = login;
    }

    public LoginResponse(bool login)
    {
        Login = login;
    }
}
