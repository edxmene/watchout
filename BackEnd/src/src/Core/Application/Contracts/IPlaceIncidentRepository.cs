// <copyright file="IPlaceIncidentRepository.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Application.Contracts.Common;
using Domain.Entities;
using Domain.Models;

namespace Application.Contracts;

public interface IPlaceIncidentRepository : IRepositoryBase<PlaceIncidentModel>
{
    Task<PlaceIncidentModel> GetPlaceIncidentAsync(string id, CancellationToken cancellationToken = default);
    Task<List<PlaceIncidentModel>> GetPlaceIncidentByNear(double latitud, double longitud, int maxDistance, int minDistance, CancellationToken cancellationToken = default);
    Task<List<PlaceIncidentModel>> GetPlaceIncidentByIntersects(double latitud, double longitud, CancellationToken cancellationToken = default);
    Task<List<PlaceIncidentModel>> GetPlaceIncidentByWithin(List<Coordinates> coordinates, CancellationToken cancellationToken = default);
    Task<List<PlaceIncidentModel>> GetPlaceIncidentByWithinCenter(double latitud, double longitud, int distance, CancellationToken cancellationToken = default);
    Task<List<PlaceIncidentModel>> GetPlaceIncidentByWithinCenterSphere(double latitud, double longitud, int distance, CancellationToken cancellationToken = default);
}
