// <copyright file="IRepositoryBase.cs" company="Watchout">
// Copyright (c) 2022</copyright>

namespace Application.Contracts.Common;

public interface IRepositoryBase<T> where T : class
{
    Task<List<T>> GetAll(CancellationToken cancellationToken = default);
    Task<T> Add(T entity, CancellationToken cancellationToken = default);
    void Update(T entity);
    void Delete(T entity);
}
