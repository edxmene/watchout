// <copyright file="IUserRepository.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Application.Contracts.Common;
using Domain.Models;

namespace Application.Contracts;

public interface IUserRepository : IRepositoryBase<UserModel>
{
    Task<UserModel> GetUserAsync(string email, CancellationToken cancellationToken = default);
}
