// <copyright file="SendEmail.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using System.Net.Mail;

namespace Application.Helpers;
public class SendEmail
{
    // Temporary credentials
    readonly string SourceEmail = "whatchoutapp@gmail.com";
    readonly string Password = "jalasoft";

    public async Task SendEmailsAsync(string destinationEmail, string token)
    {
        var tokenUrl = "http://localhost:4200/password-recovery-step2/" + token;
        string emailBody = @"<div>
                              <p>
                              Hello: <br/>
                              We see that you have forgotten your password to login in Watchout. To change it please click on the link below and follow the instructions:</p>
                              <p>#tokenUrl#</p>
                              <p>See you next time!<br/>
                              <b>The Watchout team</b></p>
                            </div>";

        emailBody =  emailBody.Replace("#tokenUrl#", tokenUrl);
        var emailMessage = new MailMessage(SourceEmail, destinationEmail, "Recovery password",
        emailBody);
        emailMessage.IsBodyHtml = true;

        using (var smtpClient = new SmtpClient("smtp.gmail.com")
        {
            EnableSsl = true,
            UseDefaultCredentials = false,
            Port = 587,
            Credentials = new System.Net.NetworkCredential(SourceEmail, Password)
        })
        {
            await smtpClient.SendMailAsync(emailMessage).ConfigureAwait(true);
            smtpClient.Dispose();
        }
    }
}
