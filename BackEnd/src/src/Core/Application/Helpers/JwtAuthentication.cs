using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using Domain.Models;

namespace Application.Helpers;

public class JwtAuthentication
{
    public string SecurityKey { get; set; } = "ouNtF8Xds1jE55/d+iVZ99u0f2U6lQ+AHdiPFwjVW3o=";
    public string ValidIssuer { get; set; } = "https://localhost:7105/";
    public string ValidAudience { get; set; } = "https://localhost:7105/";
    public SymmetricSecurityKey SymmetricSecurityKey => new SymmetricSecurityKey(Convert.FromBase64String(SecurityKey));
    public SigningCredentials SigningCredentials => new SigningCredentials(SymmetricSecurityKey, SecurityAlgorithms.HmacSha256);

    public string GenerateToken(UserModel user)
    {
        var token = new JwtSecurityToken(
            issuer: this.ValidIssuer,
            audience: this.ValidAudience,
            claims: new[]
            {
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
            },
            expires: DateTime.UtcNow.AddDays(30),
            notBefore: DateTime.UtcNow,
            signingCredentials: this.SigningCredentials);

        return new JwtSecurityTokenHandler().WriteToken(token);
    }

    public string DecodeToken(string authToken)
    {
        var handler = new JwtSecurityTokenHandler();
        var jwtSecurityToken = handler.ReadJwtToken(authToken);
        var email = jwtSecurityToken.Claims.First(claim => claim.Type == "email").Value;

        return email;
    }
}
