// <copyright file="UserController.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Microsoft.AspNetCore.Mvc;
using Application.DTOs;
using Application.Services.Contracts;
using Domain.Responses;

namespace Web.Api.Controllers;

[Route("api/[controller]")]
[ApiController]
public class UserController : ControllerBase
{

    //TODO: Implement application/services layer
    private readonly IUserService _userService;

    public UserController(IUserService userService)
    {
        _userService = userService;
    }

    [HttpGet]
    public async Task<ActionResult<UserDTO>> GetAll()
    {
        var users = await _userService.GetAllUsersAsync().ConfigureAwait(true);
        return Ok(users);
    }

    [HttpGet("email")]
    public async Task<ActionResult<UserDTO>> GetOneUser(string email)
    {
        var user = await _userService.GetUserAsync(email).ConfigureAwait(true);
        return Ok(user);
    }

    [HttpPost]
    public async Task<ActionResult> AddUser([FromBody] UserDTO userDto)
    {
        var user = await _userService.GetUserAsync(userDto.Email).ConfigureAwait(true);
        if (user == null)
        {
            var newUser = await _userService.AddUserAsync(userDto).ConfigureAwait(true);
            return Created("api/user", new UserResponse(201, newUser.Id, newUser.AuthToken, "Created successfull"));

        }
        else
        {
            return Conflict(new UserResponse(401, "User already exits."));
        }
    }

    [HttpPost]
    [Route("/api/[controller]/loginGoogle")]
    public async Task<ActionResult> RegisterGoogle([FromBody] UserGoogleDTO userGoogleDto)
    {
        var user = await _userService.GetUserAsync(userGoogleDto.Email).ConfigureAwait(true);
        if (user == null)
        {
            _ = _userService.AddUserGoogleAsync(userGoogleDto);
            return Created("api/user", new UserResponse(201, userGoogleDto.FirstName, userGoogleDto.Email, "Created successfull"));

        }
        else
        {
            return Ok(new UserResponse(200, "User already exits."));
        }
    }

    [HttpPost]
    [Route("/api/[controller]/login")]
    public async Task<ActionResult> Login([FromBody] LoginDTO userLogin)
    {
        var response = await _userService.LoginUserAsync(userLogin).ConfigureAwait(true);
        if (response.Login == false)
        {
            return Unauthorized(new UserResponse(401, "User and password wrong. Please check the email address or password."));
        }
        return Ok(new UserResponse(200, response.Id, response.AuthToken, "User logged in successfully."));

    }

    
    [HttpDelete("email")]
    public async Task<ActionResult> DeleteUser( string email)
    {
        var response = await _userService.DeleteUserAsync(email).ConfigureAwait(true);
        if (response.Result == true)
        {
            return Ok(new UserResponse(200, response.Message));

        }
       return NotFound(new UserResponse(404, response.Message));

    }

    [HttpPut("email")]
    public async Task<ActionResult> UpdateUser(string email, [FromBody] UserDTO userDto)
    {
        var response = await _userService.UpdateUserAsync(email, userDto).ConfigureAwait(true);
        if (response.Result == true)
        {
            return Ok(new UserResponse(200, response.Message));

        }
        return NotFound(new UserResponse(404, response.Message));

    }

    [HttpPost]
    [Route("/api/[controller]/send-recovery-email")]
    public async Task<ActionResult> RecoveryPassword([FromBody] SendEmailDTO email)
    {
        var response = await _userService.RecoveryPasswordAsync(email).ConfigureAwait(true);
        if (response.Result == false)
        {
            return NotFound(new UserResponse(404, response.Message));
        }
        else
        {
            return Ok(new UserResponse(200, response.Message));
        }
    }

    [HttpPost]
    [Route("/api/[controller]/reset-password/{authToken}")]
    public async Task<ActionResult<UserDTO>> ResetPassword(string authToken, [FromBody] ResetPasswordDTO newPassword)
    {
        var response = await _userService.ResetPasswordAsync(authToken, newPassword).ConfigureAwait(true);
        if (response.Result == false)
        {
            return BadRequest(new UserResponse(400, "Something went wrong"));
        }
        else
        {
            return Ok(new UserResponse(200, response.Message));
        }
    }
}
