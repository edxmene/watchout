// <copyright file="Program.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Application.Services;
using Application.Services.Contracts;
using Persistence.Repositories;
using Application.Contracts;
using FluentValidation.AspNetCore;
using Identity.Validators;
using FluentValidation;
using Application.DTOs;
using GraphQL;
using GraphQL.Types.PlaceIncident;
using MongoDB.Bson.Serialization;
using Domain.Entities;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddControllers();
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IPlaceIncidentRepository, PlaceIncidentRepository>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IPlaceIncidentService, PlaceIncidentService>();

//Injection Repositories to work with MongoDB
BsonClassMap.RegisterClassMap<User>(cm =>
{
    cm.AutoMap();
    cm.SetIgnoreExtraElements(true);
    cm.MapIdMember(c => c.Id);
});

builder.Services.RegisterMongoDbRepositories();
builder.Services.AddAuthorization();

// Validation with fluent 
builder.Services.AddMvc().AddFluentValidation();
builder.Services.AddTransient<IValidator<UserDTO>, UserDtoValidator>();
builder.Services.AddTransient<IValidator<PlaceIncidentInput>, PlaceIncidentTypeValidator>();
builder.Services.AddTransient<IValidator<ResetPasswordDTO>, ResetPasswordDTOValidator>();
builder.Services.AddTransient<IValidator<SendEmailDTO>, SendEmailDTOValidator>();
builder.Services.AddCors();

//GraphQL configuration
builder.Services.AddFluentValidation();
builder.Services.AddGraphQLServer()
                .AddQueryType<Query>()
                .AddMutationType<Mutation>()
                .AddSubscriptionType<Subscription>()
                .AddType<PlaceIncidentType>()
                .AddProjections()
                .AddFiltering()
                .AddSorting()
                .AddInMemorySubscriptions();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(options =>
{
    options.AllowAnyOrigin();
    options.AllowAnyMethod();
    options.AllowAnyHeader();
});

app.UseWebSockets();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapGraphQL("/graphql");

app.MapControllers();

app.UseGraphQLVoyager();

app.Run();
