// <copyright file="PasswordDtoValidator.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Application.DTOs;
using FluentValidation;

namespace Identity.Validators;
public class ResetPasswordDTOValidator : AbstractValidator<ResetPasswordDTO>
{
    public ResetPasswordDTOValidator()
    {
        //Password:
        /* 8 characters min
         * One lower case
         * One upper case min
         * One special character minimum (!#$%&@)             
         */
        RuleFor(passwordDto => passwordDto.NewPassword)
        .NotNull()
        .Matches(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$");
    }
}
