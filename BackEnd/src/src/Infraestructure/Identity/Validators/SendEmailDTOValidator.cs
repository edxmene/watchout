// <copyright file="SendEmailDTOValidator.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Application.DTOs;
using FluentValidation;

namespace Identity.Validators;
public class SendEmailDTOValidator : AbstractValidator<SendEmailDTO>
{
    public SendEmailDTOValidator()
    {
        //Email must be in "email format" 
        /* Text before @
         * Have one @
         * have a dot
         * Text before and after the dot (@email.com)
         */
        RuleFor(userDto => userDto.Email)
            .NotNull()
            .Matches(@"^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$");
    }
}
