// <copyright file="PasswordDtoValidator.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Application.DTOs;
using FluentValidation;

namespace Identity.Validators;

public class PasswordDtoValidator : AbstractValidator<PasswordDTO>
{
    public PasswordDtoValidator()
    {
        RuleFor(passwordDto => passwordDto.NewPassword)
          .NotNull()
          .Matches(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$");
    }
}
