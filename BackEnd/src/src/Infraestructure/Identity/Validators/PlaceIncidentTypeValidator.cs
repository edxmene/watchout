// <copyright file="PlaceIncidentTypeValidator.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using FluentValidation;
using GraphQL.Types.PlaceIncident;

namespace Identity.Validators;

public class PlaceIncidentTypeValidator : AbstractValidator<PlaceIncidentInput>
{
    public PlaceIncidentTypeValidator()
    {
        RuleFor(placeIncidentInput => placeIncidentInput.Description)
            .NotNull()
            .MinimumLength(5)
            .MaximumLength(100);

        RuleFor(placeIncidentInput => placeIncidentInput.Severity)
            .NotNull()
            .IsInEnum();

        RuleFor(placeIncidentInput => placeIncidentInput.DurationTime)
            .NotNull()
            .GreaterThan(10) //In minutes?
            .LessThan(180);

        RuleFor(placeIncidentInput => placeIncidentInput.Category)
            .NotNull()
            .IsInEnum();

        RuleFor(placeIncidentInput => placeIncidentInput.Location)
            .NotNull();
    }
}
