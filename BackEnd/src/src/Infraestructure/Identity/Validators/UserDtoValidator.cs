// <copyright file="UserDtoValidator.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using FluentValidation;
using Application.DTOs;

namespace Identity.Validators;

public class UserDtoValidator : AbstractValidator<UserDTO>
{
    public UserDtoValidator()
    {
        //FirstName must be only letters 

        RuleFor(userDto => userDto.FirstName)
            .NotNull()
            .Length(2, 20)
            .Matches(@"^[a-zA-Z]+$");

        //FirstName must be only letters 

        RuleFor(userDto => userDto.LastName)
            .NotNull()
            .Length(2, 20)
            .Matches(@"^[a-zA-Z]+$");

        //Email must be in "email format" 
        /* Text before @
         * Have one @
         * have a dot
         * Text before and after the dot (@email.com)
         */

        RuleFor(userDto => userDto.Email)
            .NotNull()
            .Matches(@"^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$");

        //Password:
        /* 8 characters min
         * One lower case
         * One upper case min
         * One special character minimum (!#$%&@)             
         */

        RuleFor(userDto => userDto.Password)
           .NotNull()
           .Matches(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$");

        RuleFor(userDto => userDto.NewPassword)
           .Matches(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$");
    }
}
