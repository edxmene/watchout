// <copyright file="AutomapperProfile.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using AutoMapper;
using Application.DTOs;
using Domain.Models;

namespace Identity.Mappings;

public class AutomapperProfile : Profile
{
    public AutomapperProfile()
    {
        /*CreateMap<UserModel, UserDTO>().ForMember(m => m.Password, opt => opt.Ignore()).ReverseMap()*/

        CreateMap<UserModel, UserDTO>().ReverseMap();
        CreateMap<UserDTO, UserDTO>().ReverseMap();
    }
}
