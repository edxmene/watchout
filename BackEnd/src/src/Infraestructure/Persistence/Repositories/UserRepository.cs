// <copyright file="UserRepository.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using Application.Contracts;
using Domain.Models;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;

namespace Persistence.Repositories;

public class UserRepository : IUserRepository
{
    private readonly IMongoCollection<UserModel> _usersCollection;

    public UserRepository(IMongoClient mongoClient)
    {
        var camelcaseconvention = new ConventionPack { new CamelCaseElementNameConvention() };
        ConventionRegistry.Register("camelcase", camelcaseconvention, type => true);

        _usersCollection = mongoClient.GetDatabase("WatchoutDB").GetCollection<UserModel>("users");
    }

    public async Task<List<UserModel>> GetAll(CancellationToken cancellationToken = default)
    {
        
        var users = await _usersCollection.Find(_ => true).ToListAsync(cancellationToken).ConfigureAwait(true);
        return users;
    }

    public async Task<UserModel> GetUserAsync(string email, CancellationToken cancellationToken = default)
    {
        var user = await _usersCollection.Find(user => user.Email == email).FirstOrDefaultAsync(cancellationToken).ConfigureAwait(true);
        return user;
    }

    public void Delete(UserModel entity)
    {
        entity.Active = false;
        _usersCollection.ReplaceOne(u => u.Email == entity.Email, entity);
    }

    public void Update(UserModel entity)
    {
        _usersCollection.ReplaceOne(u => u.Email == entity.Email, entity);
    }

    public async Task<UserModel> Add(UserModel entity, CancellationToken cancellationToken = default)
    {
        await _usersCollection.InsertOneAsync(entity, cancellationToken: cancellationToken).ConfigureAwait(true);
        return entity;
    }

 }
