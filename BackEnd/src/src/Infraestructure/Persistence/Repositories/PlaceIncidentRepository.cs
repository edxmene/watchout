// <copyright file="PlaceIncidentRepository.cs" company="Watchout">
// Copyright (c) 2022</copyright>

using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using Domain.Models;
using Application.Contracts;
using Domain.Entities;

namespace Persistence.Repositories;

public class PlaceIncidentRepository : IPlaceIncidentRepository
{
    private readonly IMongoCollection<PlaceIncidentModel> _placeIncidentCollection;

    public PlaceIncidentRepository(IMongoClient mongoClient)
    {
        var camelcaseconvention = new ConventionPack { new CamelCaseElementNameConvention() };
        ConventionRegistry.Register("camelcase", camelcaseconvention, type => true);

        _placeIncidentCollection = mongoClient.GetDatabase("WatchoutDB").GetCollection<PlaceIncidentModel>("places");
    }

    public async Task<PlaceIncidentModel> GetPlaceIncidentAsync(string id, CancellationToken cancellationToken = default)
    {
        var placeIncident = await _placeIncidentCollection.Find(incident => incident.Id == id).FirstOrDefaultAsync(cancellationToken).ConfigureAwait(true);
        return placeIncident;
    }

    public async Task<PlaceIncidentModel> Add(PlaceIncidentModel entity, CancellationToken cancellationToken = default)
    {
        await _placeIncidentCollection.InsertOneAsync(entity, cancellationToken: cancellationToken).ConfigureAwait(true);
        return entity;
    }

    public async Task<List<PlaceIncidentModel>> GetAll(CancellationToken cancellationToken = default)
    {
        var incidents = await _placeIncidentCollection.Find(_ => true).ToListAsync(cancellationToken).ConfigureAwait(true);
        return incidents;
    }

    public void Delete(PlaceIncidentModel entity)
    {
        _placeIncidentCollection.DeleteOne(i => i.Id == entity.Id);
    }

    public void Update(PlaceIncidentModel entity)
    {
        _placeIncidentCollection.ReplaceOne(i => i.Id == entity.Id, entity);
    }

    public async Task<List<PlaceIncidentModel>> GetPlaceIncidentByNear(double latitude, double longitude, int maxDistance, int minDistance, CancellationToken cancellationToken = default)
    {
        var point = MongoDB.Driver.GeoJsonObjectModel.GeoJson.Point(MongoDB.Driver.GeoJsonObjectModel.GeoJson.Position(latitude, longitude));
        var locationQuery = new FilterDefinitionBuilder<PlaceIncidentModel>().Near(tag => tag.Location, point,
            maxDistance: maxDistance, minDistance: minDistance);
        var placesNear = await _placeIncidentCollection.Find(locationQuery).ToListAsync(cancellationToken).ConfigureAwait(true);

        return placesNear;
    }

    public async Task<List<PlaceIncidentModel>> GetPlaceIncidentByWithin(List<Coordinates> coordinates, CancellationToken cancellationToken = default)
    {
        var coordinatesPolygon = coordinates.Select(c => MongoDB.Driver.GeoJsonObjectModel.GeoJson.Position(c.latitude, c.longitude));
        var coordinates2D = new MongoDB.Driver.GeoJsonObjectModel.GeoJson2DCoordinates[coordinatesPolygon.Count()];
        for(int index=0; index<coordinatesPolygon.Count(); index++)
        {

            coordinates2D[index] = coordinatesPolygon.ToList()[index];
        }

        var builder = Builders<PlaceIncidentModel>.Filter;
        var polygon = MongoDB.Driver.GeoJsonObjectModel.GeoJson.Polygon(coordinates2D);
        var filter = builder.GeoWithin(x => x.Location, polygon);
                
        var places = await _placeIncidentCollection.Find(filter).ToListAsync(cancellationToken).ConfigureAwait(true);
        return places;
    }

    public async Task<List<PlaceIncidentModel>> GetPlaceIncidentByWithinCenter(double latitude, double longitude, int distance, CancellationToken cancellationToken = default)
    {
        var builder = Builders<PlaceIncidentModel>.Filter;
        var point = MongoDB.Driver.GeoJsonObjectModel.GeoJson.Point(MongoDB.Driver.GeoJsonObjectModel.GeoJson.Position(latitude, longitude));
        var filter = builder.GeoWithinCenter(x => x.Location, point.Coordinates.X, point.Coordinates.Y, distance);

        var places = await _placeIncidentCollection.Find(filter).ToListAsync(cancellationToken).ConfigureAwait(true);
        return places;

    }

    public async Task<List<PlaceIncidentModel>> GetPlaceIncidentByWithinCenterSphere(double latitude, double longitude, int distance, CancellationToken cancellationToken = default)
    {
        var builder = Builders<PlaceIncidentModel>.Filter;
        var point = MongoDB.Driver.GeoJsonObjectModel.GeoJson.Point(MongoDB.Driver.GeoJsonObjectModel.GeoJson.Position(latitude, longitude));
        var filter = builder.GeoWithinCenterSphere(x => x.Location, point.Coordinates.X, point.Coordinates.Y, distance);

        var places = await _placeIncidentCollection.Find(filter).ToListAsync(cancellationToken).ConfigureAwait(true);
        return places;
    }

    public async Task<List<PlaceIncidentModel>> GetPlaceIncidentByIntersects(double latitude, double longitude, CancellationToken cancellationToken = default)
    {
        var builder = Builders<PlaceIncidentModel>.Filter;
        var point = MongoDB.Driver.GeoJsonObjectModel.GeoJson.Point(MongoDB.Driver.GeoJsonObjectModel.GeoJson.Position(latitude, longitude));
        var filter = builder.GeoIntersects(x => x.Geometry, point);

        var places = await _placeIncidentCollection.Find(filter).ToListAsync(cancellationToken).ConfigureAwait(true);
        return places;
    }
}
