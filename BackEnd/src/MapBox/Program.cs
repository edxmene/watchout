// See https://aka.ms/new-console-template for more information
using RestSharp;

Console.WriteLine("Hello, World!");

string url = "https://api.mapbox.com/directions/v5/mapbox/cycling/-122.662323,45.523751;-122.662323,45.523751?steps=true&geometries=geojson&access_token=pk.eyJ1IjoianVhbmRhdmlkOTAzIiwiYSI6ImNsMWJpcXdrNDBlM3MzZHM2MmhmMDc0a2QifQ.X2xwpOqV4_ceN8ZkRFRXqA";
var client = new RestClient(url);
var request = new RestRequest();
RestResponse response = await client.GetAsync(request);
Console.WriteLine(response.Content);
Console.Read();
