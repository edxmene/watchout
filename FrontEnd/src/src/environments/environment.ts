// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://localhost:7165',
  baseurl: 'https://localhost:7165/api/user',
  mapboxtoken:
    'pk.eyJ1IjoiamExYXNvZnRkaWVnbyIsImEiOiJjbDBsbHFoYmEwZGpyM2pwbG0yY2JtaDVkIn0.gBFmS9qDOMv0KIqpv0xSXQ',
  supabase: {
    publicKey:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImFuaGdvanNrb2tjcHBhbWtwd2J5Iiwicm9sZSI6ImFub24iLCJpYXQiOjE2NDY5MjQzODEsImV4cCI6MTk2MjUwMDM4MX0.lYsiwKCKMBIdNzrH2oGsa0aWGu0Y1ozNT0UVzrb2PQY',
    url: 'https://anhgojskokcppamkpwby.supabase.co',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
