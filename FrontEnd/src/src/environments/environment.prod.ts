export const environment = {
  production: true,
  apiUrl: 'https://localhost:7165',
  baseurl: 'https://localhost:7165/api/user',
  mapboxtoken:
    'pk.eyJ1IjoiamExYXNvZnRkaWVnbyIsImEiOiJjbDBsbHFoYmEwZGpyM2pwbG0yY2JtaDVkIn0.gBFmS9qDOMv0KIqpv0xSXQ',
};
