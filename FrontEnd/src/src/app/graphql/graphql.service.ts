/* eslint-disable @typescript-eslint/member-ordering */
import { Injectable } from '@angular/core';
import { Category, Severity } from '@shared/interfaces/interfaces';
import { Apollo } from 'apollo-angular';
import { BehaviorSubject } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import {
  CREATE_PLACE_INCIDENT,
  CREATE_PLACE_INCIDENT_GEOMETRY,
  DELETE_PLACE_INCIDENT,
  GET_PLACEINCIDENT,
  GET_PLACEINCIDENTS,
  QUERY_BY_CATEGORY,
  QUERY_BY_SEVERETY,
  UPDATE_PLACE_INCIDENT,
} from './graphql';

@Injectable({
  providedIn: 'root',
})
export class GraphqlService {
  private placeIncidentsSubject = new BehaviorSubject<any[]>([]);
  placeIncidents$ = this.placeIncidentsSubject.asObservable();

  constructor(private apollo: Apollo) {}

  //TODO; Observables to save the data or work with placeIncident$

  getPlaceIncidents(): void {
    this.apollo
      .watchQuery<any>({
        query: GET_PLACEINCIDENTS,
      })
      .valueChanges.pipe(
        take(1),
        tap(({ data }) => {
          const { placeIncidents } = data;
          console.log(placeIncidents);
          this.placeIncidentsSubject.next(placeIncidents);
        })
      )
      .subscribe();
  }

  filterPlaceIncidentsById(value: string): void {
    this.apollo
      .watchQuery<any>({
        query: GET_PLACEINCIDENT,
        variables: {
          id: value,
        },
      })
      .valueChanges.pipe(
        take(1),
        tap(({ data }) => {
          const { placeIncidents } = data;
          console.log(placeIncidents);
        })
      )
      .subscribe();
  }

  filterPlaceIncidentsByCategory(value: string): void {
    this.apollo
      .watchQuery<any>({
        query: QUERY_BY_CATEGORY,
        variables: {
          category: value,
        },
      })
      .valueChanges.pipe(
        take(1),
        tap(({ data }) => {
          const { placeIncidents } = data;
          console.log(placeIncidents);
        })
      )
      .subscribe();
  }

  filterPlaceIncidentsBySeverety(value: string): void {
    this.apollo
      .watchQuery<any>({
        query: QUERY_BY_SEVERETY,
        variables: {
          severety: 'MEDIUM',
        },
      })
      .valueChanges.pipe(
        take(1),
        tap(({ data }) => {
          const { placeIncidents } = data;
          console.log(placeIncidents);
        })
      )
      .subscribe();
  }

  createPlaceIncidentPoint(
    category: Category,
    description: string,
    severity: Severity,
    owner: string,
    durationTime: number,
    coordinates: number[][]
  ) {
    this.apollo
      .mutate({
        mutation: CREATE_PLACE_INCIDENT,
        variables: {
          category: category,
          description: description,
          severity: severity,
          owner: owner,
          durationTime: durationTime,
          location: {
            type: 'Point',
            coordinates: coordinates,
          },
        },
      })
      .subscribe(() => {
        console.log('created');
      });
  }

  createPlaceIncidentGeometry(
    category: Category,
    description: string,
    severity: Severity,
    owner: string,
    durationTime: number,
    type: string,
    coordinates: number[][]
  ) {
    this.apollo
      .mutate({
        mutation: CREATE_PLACE_INCIDENT_GEOMETRY,
        variables: {
          category: category,
          description: description,
          severity: severity,
          owner: owner,
          durationTime: durationTime,
          geometry: {
            type: type,
            coordinates: coordinates,
          },
        },
      })
      .subscribe(() => {
        console.log('created');
      });
  }

  updatePlaceIncident(
    id: string,
    category: Category,
    description: string,
    severity: Severity,
    owner: string,
    durationTime: number,
    type: string,
    coordinates: [number, number]
  ) {
    this.apollo
      .mutate({
        mutation: UPDATE_PLACE_INCIDENT,
        variables: {
          id: id,
          category: category,
          description: description,
          severity: severity,
          owner: owner,
          durationTime: durationTime,
          location: {
            type: type,
            coordinates: coordinates,
          },
        },
      })
      .subscribe(() => {
        console.log('update');
      });
  }

  deletePlaceIncident(id: string) {
    this.apollo
      .mutate({
        mutation: DELETE_PLACE_INCIDENT,
        variables: {
          id: id,
        },
      })
      .subscribe(res => {
        const { data } = res;
        console.log(data);
      });
  }
}
