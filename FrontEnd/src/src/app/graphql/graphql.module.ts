import { NgModule } from '@angular/core';
import { APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLink, HttpLinkHandler } from 'apollo-angular/http';
import { InMemoryCache } from '@apollo/client/core';
import { environment } from '@env/environment';

const uri = `${environment.apiUrl}/graphql`;

export const createApollo = (httpLink: HttpLink): IApollo => ({
  link: httpLink.create({ uri }),
  cache: new InMemoryCache(),
});

@NgModule({
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink],
    },
  ],
})
export class GraphQLModule {}

interface IApollo {
  link: HttpLinkHandler;
  cache: InMemoryCache;
}
