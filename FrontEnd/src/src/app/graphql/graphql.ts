import { gql } from 'apollo-angular';

export const GET_PLACEINCIDENTS = gql`
  {
    placeIncidents {
      id
      category
      description
      severity
      owner
      durationTime
      location {
        coordinates
        type
      }
    }
  }
`;

export const GET_PLACEINCIDENT = gql`
  query ($id: String) {
    placeIncidents(where: { id: { eq: $id } }) {
      id
      category
      description
      severity
      owner
      durationTime
      location {
        coordinates
        type
      }
    }
  }
`;

export const QUERY_BY_CATEGORY = gql`
  query ($category: String) {
    placeIncidents(where: { category: { eq: $category } }) {
      id
      category
      description
      severity
      owner
      durationTime
      location {
        coordinates
        type
      }
    }
  }
`;

export const QUERY_BY_SEVERETY = gql`
  query ($severety: String) {
    placeIncidents(where: { severity: { eq: $severety } }) {
      id
      category
      description
      severity
      owner
      durationTime
      location {
        coordinates
        type
      }
    }
  }
`;

export const CREATE_PLACE_INCIDENT = gql`
  mutation addPlaceIncident(
    $category: String!
    $description: String!
    $severity: String!
    $owner: String!
    $durationTime: Int!
    $location: LocationInput
  ) {
    addPlaceIncident(
      input: {
        category: $category
        description: $description
        severity: $severity
        owner: $owner
        durationTime: $durationTime
        location: $location
      }
    ) {
      placeIncident {
        category
        description
        severity
        owner
        durationTime
        location {
          coordinates
          type
        }
      }
    }
  }
`;

export const CREATE_PLACE_INCIDENT_GEOMETRY = gql`
  mutation addPlaceIncident(
    $category: String!
    $description: String!
    $severity: String!
    $owner: String!
    $durationTime: Int!
    $geometry: GeometryInput
  ) {
    addPlaceIncident(
      input: {
        category: $category
        description: $description
        severity: $severity
        owner: $owner
        durationTime: $durationTime
        geometry: $geometry
      }
    ) {
      placeIncident {
        category
        description
        severity
        owner
        durationTime
        geometry {
          coordinates
          type
        }
      }
    }
  }
`;

export const UPDATE_PLACE_INCIDENT = gql`
  mutation updatePlaceIncident(
    $id: String!
    $category: String!
    $description: String!
    $severity: String!
    $owner: String!
    $durationTime: Int!
    $location: LocationInput
  ) {
    updatePlaceIncident(
      input: {
        category: $category
        description: $description
        severity: $severity
        owner: $owner
        durationTime: $durationTime
        location: $location
      }
      id: $id
    ) {
      placeIncident {
        category
        description
        severity
        owner
        durationTime
        location {
          coordinates
          type
        }
      }
    }
  }
`;

export const DELETE_PLACE_INCIDENT = gql`
  mutation deletePlaceIncident($id: String!) {
    deletePlaceIncident(id: $id) {
      response {
        message
        result
      }
    }
  }
`;
