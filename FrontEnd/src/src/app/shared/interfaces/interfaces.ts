export interface LoginDto {
  email: string;
  password: string;
}

export interface UserDto {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  authToken: string;
}

export interface ResetPasswordDTO {
  newPassword: string;
}

export interface EmailDTO {
  email: string;
}

export enum Severity {
  HIGH = 'HIGH',
  MEDIUM = 'MEDIUM',
  LOW = 'LOW',
}

export enum Category {
  DANGER_ZONE = 'DANGER_ZONE',
  NATURAL_DISASTER = 'NATURAL_DISASTER',
  SOCIAL_INCIDENT = 'SOCIAL_INCIDENT',
  TRAFFIC_INCIDENT = 'TRAFFIC_INCIDENT',
}

export interface LocationInput {
  type: string;
  coordinates: [number, number];
}

export interface GeometryInput {
  type: string;
  coordinates: [number, number][];
}
