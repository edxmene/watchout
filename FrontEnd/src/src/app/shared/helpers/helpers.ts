import { USER_BE_STORAGE_KEY } from '@shared/constants/constants';

export const saveLocalStorage = (token: string) => {
  localStorage.setItem(USER_BE_STORAGE_KEY, token);
};
