import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AuthModule } from '@auth/auth.module';
import { MaterialUIModule } from '@material/materialUI.module';
import { MainPageModule } from '@main/main-page.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//import { GraphQLModule } from './graphql/graphql.module';
// import{DynamicTestModule};
import { ApolloModule, APOLLO_OPTIONS } from 'apollo-angular';
import { InMemoryCache } from '@apollo/client/core';
import { HttpLink } from 'apollo-angular/http';
import { environment } from '@env/environment';

const uri = `${environment.apiUrl}/graphql`;

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    //GraphQLModule,
    MainPageModule,
    MaterialUIModule,
    BrowserAnimationsModule,
    MaterialUIModule,
    ReactiveFormsModule,
    HttpClientModule,
    AuthModule,
    MainPageModule,
    HttpClientModule,
    ApolloModule,
  ],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: (httpLink: HttpLink) => {
        return {
          cache: new InMemoryCache(),
          link: httpLink.create({ uri }),
        };
      },
      deps: [HttpLink],
    },
  ],

  bootstrap: [AppComponent],
})
export class AppModule {}
