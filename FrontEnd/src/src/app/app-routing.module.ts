import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@auth/guards/auth.guard';
import { HomeGuard } from '@auth/guards/home.guard';
import { SignInComponent } from '@auth/pages/sign-in/sign-in.component';
import { SignUpComponent } from '@auth/pages/sign-up/sign-up.component';
import { PasswordRecoveryComponent } from '@auth/pages/password-recovery/password-recovery.component';
import { HomePageComponent } from '@main/pages/home-page/home-page.component';
import { PasswordRecoveryStep2Component } from '@auth/pages/password-recovery-step2/password-recovery-step2.component';

const routes: Routes = [
  { path: '', component: SignInComponent, canActivate: [AuthGuard] },
  { path: 'sign-in', component: SignInComponent, canActivate: [AuthGuard] },
  { path: 'sign-up', component: SignUpComponent, canActivate: [AuthGuard] },
  { path: 'home', component: HomePageComponent },
  {
    path: 'password-recovery',
    component: PasswordRecoveryComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'password-recovery-step2/:authToken',
    component: PasswordRecoveryStep2Component,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
