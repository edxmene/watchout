import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from '@env/environment';
import * as mapboxgl from 'mapbox-gl';

@Injectable({
  providedIn: 'root',
})
export class MapboxService {
  mapbox = mapboxgl as typeof mapboxgl;
  map!: mapboxgl.Map;
  style = 'mapbox://styles/mapbox/streets-v11';
  lat = 4.66336863727521;
  lng = -74.07231699675322;
  zoom = 13;
  mapInstances: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  marker!: any;
  constructor() {}

  createMap() {
    try {
      this.map = new mapboxgl.Map({
        container: 'map',
        style: this.style,
        zoom: this.zoom,
        center: [this.lng, this.lat],
      });
      this.mapInstances.next(this.map);
    } catch (error) {
      console.log('Something went wrong creating the map', error);
    }
  }

  createMarker(lngLat: any, map: any, location: any, localMarker: any) {
    const popup = new mapboxgl.Popup({
      closeButton: false,
      closeOnClick: false,
    });
    if (!localMarker) {
      const marker = new mapboxgl.Marker({
        color: 'red',
        draggable: true,
        scale: 2,
      })
        .setLngLat([lngLat.lng, lngLat.lat])
        .setPopup(new mapboxgl.Popup().setHTML(`<h4>${location}</h4>`))
        .addTo(map);
      return marker;
    } else {
      localMarker.remove();
      return;
    }
  }

  addLayer(map: any, source: any) {
    map.addLayer({
      id: 'places',
      type: 'circle',
      source: source,
      paint: {
        'circle-color': '#4264fb',
        'circle-radius': 8,
        'circle-stroke-width': 2,
        'circle-stroke-color': '#ffffff',
      },
    });
  }

  addSource(map: any, lngLat: any) {
    map.addSource('places', {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              description:
                '<strong>Make it Mount Pleasant</strong><p>Make it Mount Pleasant is a handmade and vintage market and afternoon of live entertainment and kids activities. 12:00-6:00 p.m.</p>',
            },
            geometry: {
              type: 'Point',
              coordinates: [+lngLat.lng, +lngLat.lat],
            },
          },
        ],
      },
    });
  }

  createMarker_(lngLat: any, map: any, location: any, popup_: mapboxgl.Popup) {
    if (map.getLayer('places')) {
      this.map.removeLayer('places');
      this.map.removeSource('places');
    }

    this.addSource(map, lngLat);
    this.addLayer(map, 'places');
    const popup = new mapboxgl.Popup({
      closeButton: true,
      closeOnClick: false,
    });

    map.on('mouseover', 'places', (e: any) => {
      popup.setLngLat(lngLat).setHTML(location).addTo(map);
      map.getCanvas().style.cursor = 'pointer';
    });
    map.on('mouseleave', 'places', () => {
      map.getCanvas().style.cursor = '';
      popup.getElement().remove();
    });
    return popup;
  }

  createCustomMarker(
    lngLat: any,
    map: any,
    layerID: string,
    sourceID: string,
    ImageID: string
  ) {
    this.removeElements(map, layerID, sourceID, ImageID);

    map.loadImage(
      'https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png',
      (error: any, image: any) => {
        if (error) throw error;
        map.addImage(ImageID, image);
      }
    );
    map.addSource(sourceID, {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features: [
          {
            // feature for Mapbox DC
            type: 'Feature',
            geometry: {
              type: 'Point',
              coordinates: [+lngLat.lng, +lngLat.lat],
            },
            properties: {},
          },
        ],
      },
    });
    // Add a symbol layer
    map.addLayer({
      id: layerID,
      type: 'symbol',
      source: sourceID,
      layout: {
        'icon-image': ImageID,
        // get the title name from the source's "title" property
        'text-field': ['get', 'title'],
        'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
        'text-offset': [0, 1.25],
        'text-anchor': 'top',
      },
    });
  }

  removeElements(map: any, layerID: string, sourceID: string, ImageID: string) {
    if (map.getLayer(layerID)) {
      this.map.removeLayer(layerID);
      this.map.removeSource(sourceID);
      if (ImageID) {
        this.map.removeImage(ImageID);
      }
    }
  }

  async reverseGeoCoding(lngLat: any) {
    const query = await fetch(
      `https://api.mapbox.com/geocoding/v5/mapbox.places/${lngLat.lng},${lngLat.lat}.json?access_token=${environment.mapboxtoken}`,
      { method: 'GET' }
    );
    const json = await query.json();
    return json.features[0].place_name;
  }
}
