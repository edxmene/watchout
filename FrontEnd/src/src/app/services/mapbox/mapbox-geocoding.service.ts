import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { LngLatLike } from 'mapbox-gl';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Feature {
  place_name: string;
  center: LngLatLike;
}

export interface MapboxOutput {
  attribution: string;
  features: Feature[];
  query: [];
}

@Injectable({
  providedIn: 'root',
})
export class MapboxGeocodingService {
  constructor(private http: HttpClient) {}

  search_word(query: string): Observable<Feature[]> {
    const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/';
    return (
      this.http
        .get<MapboxOutput>(
          `${url}${query}.json?types=place&access_token=${environment.mapboxtoken}`
        )
        // environment.mapboxtoken
        .pipe(
          map(res => {
            return res.features;
          })
        )
    );
  }
}
