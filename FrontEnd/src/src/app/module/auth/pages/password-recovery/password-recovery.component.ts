import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '@services/auth/user.service';

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.component.html',
  styleUrls: ['./password-recovery.component.scss'],
})
export class PasswordRecoveryComponent implements OnInit {
  constructor(
    private _router: Router,
    private _fb: FormBuilder,
    private readonly _userService: UserService
  ) {}
  loading = false;
  form!: FormGroup;
  subscription!: any;

  get email() {
    return this.form.get('email');
  }

  sendEmail() {
    const email = this.form.value;
    this.subscription = this._userService
      .sendRecoveryPasswordEmail(email)
      .subscribe({
        next: res => {
          this.fakeLoading();
          alert(`Check your email ${this.form.value.email}`);
        },
        error: err => {
          console.log(err);
        },
      });
  }

  fakeLoading() {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 1580);
  }

  ngOnInit(): void {
    this.form = this._fb.group({
      email: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(50),
      ]),
    });
  }
}
