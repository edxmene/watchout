import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '@services/auth/user.service';

@Component({
  selector: 'app-password-recovery-step2',
  templateUrl: './password-recovery-step2.component.html',
  styleUrls: ['./password-recovery-step2.component.scss'],
})
export class PasswordRecoveryStep2Component implements OnInit {
  loading = false;
  form!: FormGroup;
  hide_pwd = true;
  hide_confirm_pwd = true;
  subscription!: any;
  constructor(
    private _fb: FormBuilder,
    private _userService: UserService,
    private _router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.form = this._fb.group(
      {
        newPassword: new FormControl('', [
          Validators.required,
          Validators.pattern(
            /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/
          ),
        ]),
        confirmPassword: new FormControl('', [Validators.required]),
      },
      {
        validators: this.MustMatch('newPassword', 'confirmPassword'),
      }
    );
  }

  get newPassword() {
    return this.form.get('newPassword');
  }
  get confirmPassword() {
    return this.form.get('confirmPassword');
  }

  resetPassword() {
    const newPassword = { newPassword: this.form.value.newPassword };
    this.subscription = this._userService
      .resetPassword(newPassword, this.route.snapshot.params['authToken'])
      .subscribe({
        next: res => {
          this.fakeLoading();
          alert('Password changed successfully');
          this.newPassword?.setValue('');
          this.confirmPassword?.setValue('');
        },
        error: err => {
          console.log(err);
        },
      });
  }

  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (matchingControl.errors && !matchingControl.errors['MustMatch']) {
        return;
      }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ MustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

  fakeLoading() {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 1580);
  }
}
