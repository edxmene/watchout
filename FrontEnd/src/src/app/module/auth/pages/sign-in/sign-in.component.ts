import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { saveLocalStorage } from '@shared/helpers/helpers';
import { UserService } from '@services/auth/user.service';
import { GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  loading = false;
  subscription!: any;
  hide = true;
  constructor(
    private _router: Router,
    private _fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private readonly _userService: UserService,
    private authService: SocialAuthService
  ) {}

  ngOnInit(): void {
    this.form = this._fb.group({
      email: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(100),
        Validators.email,
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20),
      ]),
    });
  }

  //Getters to Form
  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }

  //Login
  signIn() {
    const user = this.form.value;
    this.subscription = this._userService.login(user).subscribe({
      next: res => {
        this.fakeLoading();
        saveLocalStorage(res.authToken);
        this._router.navigateByUrl('/home');
      },
      error: err => {
        this.error(err.error.message);
      },
    });
  }

  onLoginGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(data => {
      localStorage.setItem('google_auth', JSON.stringify(data));
      this._router.navigateByUrl('/home').then();
    });
  }

  //Helpers to login

  fakeLoading() {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 1580);
  }

  error(message: string) {
    this._snackBar.open(message, '', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }

  private redirectUser(): void {
    this._router.navigate(['/home']);
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }
}
