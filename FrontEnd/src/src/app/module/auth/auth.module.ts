import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialUIModule } from '@material/materialUI.module';
import { SignInComponent } from './pages/sign-in/sign-in.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import {
  GoogleLoginProvider,
  SocialAuthServiceConfig,
  SocialLoginModule,
} from 'angularx-social-login';
import { PasswordRecoveryComponent } from './pages/password-recovery/password-recovery.component';
import { PasswordRecoveryStep2Component } from './pages/password-recovery-step2/password-recovery-step2.component';

@NgModule({
  declarations: [
    SignInComponent,
    SignUpComponent,
    PasswordRecoveryComponent,
    PasswordRecoveryStep2Component,
  ],
  imports: [
    CommonModule,
    MaterialUIModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    SocialLoginModule,
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '355653292753-h4pcukh7577dm5d0d97tm9gkfv2gv8bs.apps.googleusercontent.com' // add web app client id
            ),
          },
        ],
      } as SocialAuthServiceConfig,
    },
  ],
})
export class AuthModule {}
