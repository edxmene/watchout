import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import {
  USER_BE_STORAGE_KEY,
  USER_STORAGE_KEY,
} from '@shared/constants/constants';

@Injectable({ providedIn: 'root' })
export class HomeGuard implements CanActivate {
  constructor(private readonly router: Router) {}
  canActivate(): boolean {
    if (
      !localStorage.getItem(USER_STORAGE_KEY) &&
      !localStorage.getItem(USER_BE_STORAGE_KEY)
    ) {
      this.router.navigate(['/sign-in']);
      return false;
    }
    return true;
  }
}
