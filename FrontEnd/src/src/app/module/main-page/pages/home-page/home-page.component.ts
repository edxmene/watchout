import { Component } from '@angular/core';
import { Feature } from '@services/mapbox/mapbox-geocoding.service';
import { Category, Severity } from '@shared/interfaces/interfaces';
import { GraphqlService } from 'app/graphql/graphql.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent {
  showSideBar: boolean = false;
  eventType!: string;

  selectedFeature: Feature | undefined = undefined;

  constructor(private graphqlSvc: GraphqlService) {}

  //TODO: Implement delete functionality
  // deletePlaceIncident(): void {
  //   this.graphqlSvc.deletePlaceIncident('62558a6dfb51b61ff5829299');
  // }

  onTextChange(text: string | Event) {
    console.log(text);
  }

  onPlaceSelected(feature: Feature) {}
}
