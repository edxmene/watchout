import { StringMap } from '@angular/compiler/src/compiler_facade_interface';
import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { ComponentsService } from '@services/component/components.service';
import { BottomBarComponent } from '../bottom-bar/bottom-bar.component';
import { eventDialFabAnimations } from './event-fab.animations';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MapViewComponent } from '../map-view/map-view.component';

interface IButtons {
  icon: string;
}

@Component({
  selector: 'app-event-fab',
  templateUrl: './event-fab.component.html',
  styleUrls: ['./event-fab.component.scss'],
  animations: eventDialFabAnimations,
})
export class EventFabComponent implements OnInit, OnDestroy {
  fabButtons: IButtons[] = [
    {
      icon: 'map',
    },
    {
      icon: 'motion_photos_on',
    },
    {
      icon: 'moving',
    },
    {
      icon: 'location_on',
    },
  ];
  buttons: IButtons[] = [];
  fabTogglerState = 'inactive';
  eventBarStatusSubs: any;
  @Output() clickEvent = new EventEmitter<string>();

  constructor(
    private componentsService: ComponentsService,
    private _bottomSheet: MatBottomSheet
  ) {}

  ngOnInit(): void {
    this.eventBarStatusSubs = this.componentsService.eventBarStatus.subscribe(
      arrayOfIcons => {
        this.buttons = arrayOfIcons;
      }
    );
  }

  ngOnDestroy(): void {
    this.eventBarStatusSubs.unsubscribe();
  }
  showItems() {
    this.fabTogglerState = 'active';
    this.buttons = this.fabButtons;
  }

  hideItems() {
    this.fabTogglerState = 'inactive';
    this.buttons = [];
  }

  onToggleFab() {
    this.buttons.length ? this.hideItems() : this.showItems();
  }

  openBottomSheet(eventType: any) {
    this._bottomSheet.open(BottomBarComponent, {
      hasBackdrop: false,
      data: eventType,
    });
    this.componentsService.eventBarStatus.next([]);
  }
}
