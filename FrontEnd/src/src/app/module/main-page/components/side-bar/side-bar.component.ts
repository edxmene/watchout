import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {
  USER_BE_STORAGE_KEY,
  USER_STORAGE_KEY,
} from '@shared/constants/constants';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss'],
})
export class SideBarComponent {
  constructor(private _router: Router) {}

  logout(): void {
    localStorage.removeItem(USER_STORAGE_KEY);
    localStorage.removeItem(USER_BE_STORAGE_KEY);
    this._router.navigateByUrl('/sign-in').then();
  }
}
