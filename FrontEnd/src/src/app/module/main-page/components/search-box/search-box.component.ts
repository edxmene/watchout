import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ComponentsService } from '@services/component/components.service';

import {
  MapboxGeocodingService,
  Feature,
} from '@services/mapbox/mapbox-geocoding.service';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss'],
})
export class SearchBoxComponent {
  private sideBarValue: boolean = false;

  @Input() sideBar!: boolean;

  public getSideBar(): boolean {
    return this.sideBarValue;
  }

  @Output() onMenuButtonClick = new EventEmitter();

  selectedFeatureValue: Feature | undefined = undefined;

  @Output() selectedFeatureChange: EventEmitter<Feature | undefined> =
    new EventEmitter();

  @Input()
  get selectedFeature(): Feature | undefined {
    return this.selectedFeatureValue;
  }

  set selectedFeature(feature: Feature | undefined) {
    this.selectedFeatureValue = feature;
    this.selectedFeatureChange.emit(this.selectedFeatureValue);
  }

  selectedAddressValue: string = '';

  get selectedAddress(): string {
    return this.selectedAddressValue;
  }

  set selectedAddress(value: string) {
    this.selectedAddressValue = value;
    this.searchPlace();
  }

  showAdresses: boolean = false;

  constructor(
    private mapboxService: MapboxGeocodingService,
    private componenService: ComponentsService
  ) {}

  features: Feature[] = [];

  searchPlace() {
    const searchTerm = `${this.selectedAddress.toLowerCase()}`;
    this.showAdresses = true;

    if (searchTerm.length > 0) {
      this.mapboxService
        .search_word(searchTerm)
        .subscribe((features: Feature[]) => {
          this.features = features;
        });
    } else {
      console.log('else');
      this.features = [];
    }
  }

  onSelect(feature: Feature) {
    console.log('feature', feature.center);
    this.selectedAddress = feature.place_name;
    this.selectedFeature = feature;
    this.features = [];
    this.showAdresses = false;
  }

  onBlur(event: any) {
    setTimeout(() => {
      this.showAdresses = false;
    }, 150);
  }

  onMenuClick() {
    this.onMenuButtonClick.emit();
    this.componenService.eventCloseNavBottomMenu.next(true);
  }
}
