import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventPolygonComponent } from './event-polygon.component';

describe('EventPolygonComponent', () => {
  let component: EventPolygonComponent;
  let fixture: ComponentFixture<EventPolygonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EventPolygonComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventPolygonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
