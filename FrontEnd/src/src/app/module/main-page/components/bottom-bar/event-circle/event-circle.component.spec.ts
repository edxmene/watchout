import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventCircleComponent } from './event-circle.component';

describe('EventCircleComponent', () => {
  let component: EventCircleComponent;
  let fixture: ComponentFixture<EventCircleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EventCircleComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCircleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
