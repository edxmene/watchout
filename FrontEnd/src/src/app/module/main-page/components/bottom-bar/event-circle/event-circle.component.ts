import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { MapboxService } from '@services/mapbox/mapbox.service';
import { GraphqlService } from 'app/graphql/graphql.service';

@Component({
  selector: 'app-event-circle',
  templateUrl: './event-circle.component.html',
  styleUrls: ['./event-circle.component.scss'],
})
export class EventCircleComponent implements OnInit, OnDestroy {
  @Input() categories: any;
  @Input() severities: any;
  @Input() location: any;
  signupForm!: FormGroup;
  sliderValue: any = 200;
  map_!: any;
  event!: any;
  eventCircle: boolean = false;
  public set sliderValue_(val: any) {
    this.sliderValue = val;
    if (this.eventCircle) {
      this.createCircle(this.map_, this.event);
    }
  }

  public get sliderValue_() {
    return this.sliderValue;
  }
  constructor(
    private graphqlSvc: GraphqlService,
    private mapService: MapboxService
  ) {}

  ngOnInit(): void {
    this.eventCircle = true;
    this.mapService.mapInstances.subscribe(map => {
      this.map_ = map;
      map.on('click', (e: any) => {
        this.event = e;
        if (this.eventCircle) {
          this.createCircle(map, e);
        }
      });
    });
  }

  createCircle(map: any, e: any) {
    if (map.getLayer('circle')) {
      map.removeLayer('circle');
      map.removeLayer('outline');
      map.removeSource('maine');
    }

    const poly = this.createGeoJSONCircle(
      [e.lngLat.lng, e.lngLat.lat],
      this.sliderValue / 1000
    );
    map.addSource('maine', {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'Polygon',
              coordinates: [poly],
            },
          },
        ],
      },
    });

    map.addLayer({
      // id: this.mapID.toString(),
      id: 'circle',
      type: 'fill',
      source: 'maine', // reference the data source
      layout: {},
      paint: {
        'fill-color': '#ff7846', // primary color fill
        'fill-opacity': 0.5,
      },
    });

    map.addLayer({
      id: 'outline',
      type: 'line',
      source: 'maine',
      layout: {},
      paint: {
        'line-color': 'black',
        'line-width': 2,
      },
    });
  }

  onSubmit(form: NgForm) {
    console.log(form.value);
  }

  createGeoJSONCircle = function (
    center: any,
    radiusInKm: any,
    points: any = 64
  ) {
    if (!points) points = 64;

    var coords = {
      latitude: center[1],
      longitude: center[0],
    };

    var km = radiusInKm;

    var ret = [];
    var distanceX = km / (111.32 * Math.cos((coords.latitude * Math.PI) / 180));
    var distanceY = km / 110.574;

    var theta, x, y;
    for (var i = 0; i < points; i++) {
      theta = (i / points) * (2 * Math.PI);
      x = distanceX * Math.cos(theta);
      y = distanceY * Math.sin(theta);

      ret.push([coords.longitude + x, coords.latitude + y]);
    }
    ret.push(ret[0]);

    return ret;
  };

  ngOnDestroy(): void {
    if (this.map_.getLayer('circle')) {
      this.map_.removeLayer('circle');
      this.map_.removeLayer('outline');
      this.map_.removeSource('maine');
    }
    this.eventCircle = false;
  }
}
