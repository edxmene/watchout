import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import {
  MatBottomSheetRef,
  MAT_BOTTOM_SHEET_DATA,
} from '@angular/material/bottom-sheet';
import { ComponentsService } from '@services/component/components.service';
import { Category, Severity } from '@shared/interfaces/interfaces';

@Component({
  selector: 'app-bottom-bar',
  templateUrl: './bottom-bar.component.html',
  styleUrls: ['./bottom-bar.component.scss'],
})
export class BottomBarComponent implements OnInit {
  categories: any = [
    { id: Category.DANGER_ZONE, name: 'Danger Zone' },
    { id: Category.NATURAL_DISASTER, name: 'Natural Disaster' },
    { id: Category.SOCIAL_INCIDENT, name: 'Social Incident' },
    { id: Category.TRAFFIC_INCIDENT, name: 'Traffic Incident' },
  ];

  severities: any = [
    { id: Severity.HIGH, name: 'High' },
    { id: Severity.MEDIUM, name: 'Medium' },
    { id: Severity.LOW, name: 'Low' },
  ];

  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public eventType: any,
    private _bottomSheetRef: MatBottomSheetRef<BottomBarComponent>,
    private componentService: ComponentsService
  ) {}

  ngOnInit(): void {
    this.componentService.eventCloseNavBottomMenu.subscribe(close => {
      if (close) {
        this.onCloseMenu();
      }
    });
  }

  onCloseMenu() {
    this._bottomSheetRef.dismiss();
    this.componentService.eventCloseNavBottomMenu.next(false);
  }
}
