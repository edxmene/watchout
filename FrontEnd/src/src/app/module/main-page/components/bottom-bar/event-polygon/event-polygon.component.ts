import {
  AfterViewInit,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import MapboxDraw from '@mapbox/mapbox-gl-draw';
import { MapboxService } from '@services/mapbox/mapbox.service';
import { GraphqlService } from 'app/graphql/graphql.service';

@Component({
  selector: 'app-event-polygon',
  templateUrl: './event-polygon.component.html',
  styleUrls: ['./event-polygon.component.scss'],
})
export class EventPolygonComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() categories: any;
  @Input() severities: any;
  @Input() location: any;
  signupForm!: FormGroup;
  map!: any;
  draw: any;
  constructor(
    private graphqlSvc: GraphqlService,
    private mapService: MapboxService
  ) {}

  ngOnInit(): void {
    this.signupForm = new FormGroup({
      location: new FormControl(null, Validators.required),
      category: new FormControl(null, Validators.required),
      severity: new FormControl(null, Validators.required),
      duration: new FormControl(null, Validators.required),
      description: new FormControl(null, [
        Validators.required,
        Validators.min(5),
      ]),
    });

    this.mapService.mapInstances.subscribe(map => {
      this.map = map;
    });
    this.draw = new MapboxDraw({
      displayControlsDefault: false,
      controls: {
        polygon: true,
        trash: true,
      },
      defaultMode: 'draw_polygon',
    });
    this.map.addControl(this.draw);
  }

  ngAfterViewInit(): void {
    this.map.on('draw.create', (event: any) => {
      console.log(event.features[0].geometry.coordinates);
    });
    this.map.on('draw.delete', (event: any) => {
      console.log(event);
    });
    this.map.on('draw.update', (event: any) => {
      console.log(event);
    });
  }

  onSubmit() {
    console.log(this.signupForm.value);
  }

  ngOnDestroy(): void {
    this.map.removeControl(this.draw);
    this.map.off('draw.create');
    this.map.off('draw.delete');
    this.map.off('draw.update');
  }
}
