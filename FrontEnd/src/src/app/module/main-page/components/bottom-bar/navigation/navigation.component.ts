import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MapboxService } from '@services/mapbox/mapbox.service';
import { GraphqlService } from 'app/graphql/graphql.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit, OnDestroy {
  @Input() categories: any;
  @Input() severities: any;
  @Input() location: any;
  signupForm!: FormGroup;
  map!: any;
  navigation: boolean = false;

  constructor(
    private graphqlSvc: GraphqlService,
    private mapService: MapboxService
  ) {}

  ngOnInit(): void {
    this.navigation = true;
    this.signupForm = new FormGroup({
      location: new FormControl(null, Validators.required),
      category: new FormControl(null, Validators.required),
      severity: new FormControl(null, Validators.required),
      duration: new FormControl(null, Validators.required),
      description: new FormControl(null, [
        Validators.required,
        Validators.min(5),
      ]),
    });

    this.mapService.mapInstances.subscribe(map => {
      this.map = map;
      map.on('click', (e: any) => {
        this.location = [+e.lngLat.lng.toFixed(4), +e.lngLat.lat.toFixed(4)];
        if (this.navigation) {
          this.mapService.createCustomMarker(
            e.lngLat,
            map,
            'points',
            'points',
            'custom-image'
          );
        }
      });
    });
  }

  onSubmit() {
    console.log(this.signupForm.value);
    this.graphqlSvc.createPlaceIncidentPoint(
      this.signupForm.value.category,
      this.signupForm.value.description,
      this.signupForm.value.severity,
      'owner',
      +this.signupForm.value.duration,
      this.location
    );
  }

  ngOnDestroy(): void {
    this.mapService.removeElements(
      this.map,
      'points',
      'points',
      'custom-image'
    );
    this.navigation = false;
  }
}
