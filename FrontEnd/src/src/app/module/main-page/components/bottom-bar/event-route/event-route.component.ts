import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { environment } from '@env/environment';
import { MapboxService } from '@services/mapbox/mapbox.service';
import { GraphqlService } from 'app/graphql/graphql.service';

@Component({
  selector: 'app-event-route',
  templateUrl: './event-route.component.html',
  styleUrls: ['./event-route.component.scss'],
})
export class EventRouteComponent implements OnInit, OnDestroy {
  @Input() categories: any;
  @Input() severities: any;
  @Input() location: any;
  signupForm!: FormGroup;
  modeInput = 'none';
  map!: any;
  eventRoute: boolean = false;
  wayPoints: any = { start: null, end: null };
  constructor(
    private graphqlSvc: GraphqlService,
    private mapService: MapboxService
  ) {}

  ngOnInit(): void {
    this.eventRoute = true;
    this.signupForm = new FormGroup({
      location: new FormControl(null, Validators.required),
      category: new FormControl(null, Validators.required),
      severity: new FormControl(null, Validators.required),
      duration: new FormControl(null, Validators.required),
      description: new FormControl(null, [
        Validators.required,
        Validators.min(5),
      ]),
    });

    this.mapService.mapInstances.subscribe(map => {
      this.map = map;
      map.on('click', (e: any) => {
        if (this.eventRoute) {
          if (this.modeInput === 'start') {
            this.wayPoints.start = e.lngLat;
          }
          if (this.modeInput === 'end') {
            this.wayPoints.end = e.lngLat;
          }
          if (this.wayPoints.start && this.wayPoints.end) {
            this.getRoute(this.wayPoints.start, this.wayPoints.end, map);
            console.log('HEELLOOOOO');
          }
        }
      });
    });
  }

  onSubmit() {
    console.log(this.signupForm.value);
    this.graphqlSvc.createPlaceIncidentPoint(
      this.signupForm.value.category,
      this.signupForm.value.description,
      this.signupForm.value.severity,
      'owner',
      +this.signupForm.value.duration,
      this.location
    );
  }

  async getRoute(start: any, end: any, map: any) {
    if (map.getLayer('route')) {
      map.removeLayer('route');
      map.removeSource('route');
    }
    const query = await fetch(
      `https://api.mapbox.com/directions/v5/mapbox/driving-traffic/${start.lng},${start.lat};${end.lng},${end.lat}?geometries=geojson&access_token=${environment.mapboxtoken}`,
      { method: 'GET' }
    );
    const json = await query.json();
    const data = json.routes[0];
    const route = data.geometry.coordinates;
    console.log(route);

    map.addSource('route', {
      type: 'geojson',
      data: {
        type: 'Feature',
        properties: {},
        geometry: {
          type: 'LineString',
          coordinates: route,
        },
      },
    });

    map.addLayer({
      id: 'route',
      type: 'line',
      source: 'route',
      layout: {
        'line-join': 'round',
        'line-cap': 'round',
      },
      paint: {
        'line-color': '#ff7846',
        'line-width': 5,
        'line-opacity': 0.95,
      },
    });
  }

  changeMode(mode: string) {
    this.modeInput = mode;
  }

  ngOnDestroy(): void {
    this.map.removeLayer('route');
    this.map.removeSource('route');
    this.eventRoute = false;
  }
}
