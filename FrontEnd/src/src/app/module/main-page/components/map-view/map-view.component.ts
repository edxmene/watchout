import { Component, OnInit } from '@angular/core';
import { MapboxService } from '@services/mapbox/mapbox.service';

import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.scss'],
})
export class MapViewComponent implements OnInit {
  constructor(private mapService: MapboxService) {}

  ngOnInit(): void {
    this.mapService.createMap();
  }
}
