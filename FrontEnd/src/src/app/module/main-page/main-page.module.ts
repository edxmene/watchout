import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventFabComponent } from './components/event-fab/event-fab.component';
import { SearchBoxComponent } from './components/search-box/search-box.component';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { MapViewComponent } from './components/map-view/map-view.component';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { MaterialUIModule } from '@material/materialUI.module';
import { BottomBarComponent } from './components/bottom-bar/bottom-bar.component';
import { NavigationComponent } from './components/bottom-bar/navigation/navigation.component';
import { EventRouteComponent } from './components/bottom-bar/event-route/event-route.component';
import { EventCircleComponent } from './components/bottom-bar/event-circle/event-circle.component';
import { EventPolygonComponent } from './components/bottom-bar/event-polygon/event-polygon.component';

let token =
  'pk.eyJ1IjoiamExYXNvZnRkaWVnbyIsImEiOiJjbDBsbHFoYmEwZGpyM2pwbG0yY2JtaDVkIn0.gBFmS9qDOMv0KIqpv0xSXQ';

@NgModule({
  declarations: [
    MapViewComponent,
    SearchBoxComponent,
    SideBarComponent,
    HomePageComponent,
    EventFabComponent,
    BottomBarComponent,
    NavigationComponent,
    EventRouteComponent,
    EventCircleComponent,
    EventPolygonComponent,
  ],
  imports: [
    CommonModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    NgxMapboxGLModule.withConfig({
      accessToken: token, // Optional, can also be set per map (accessToken input of mgl-map)
    }),
    MaterialUIModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    MapViewComponent,
    SearchBoxComponent,
    SideBarComponent,
    HomePageComponent,
  ],
})
export class MainPageModule {}
